$(document).ready(function () {
    $('.tabs .tab').click(function() {
        $('.tabs .tab').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('id');
        $('.tab_change').fadeOut( 0, function() { });
        $('.tab_change.id_' + id).fadeIn( 600, function() { });
    });

    $('.tabs .tab2').click(function() {
        $('.tabs .tab2').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('id');
        $('.tab_change2').fadeOut( 0, function() { });
        $('.tab_change2.id_' + id).fadeIn( 600, function() { });
    });

    $('.tabs .tab3').click(function() {
        $('.tabs .tab3').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('id');
        $('.tab_change3').fadeOut( 0, function() { });
        $('.tab_change3.id_' + id).fadeIn( 600, function() { });
    });


    $(window).scroll(function() {
        var w_h = $(window).height();
        if ($(this).scrollTop()>335){
            $('body').removeClass('no_scroll');
            $('body').addClass('scroll');

            $('.float_menu .rates_links').addClass('animated fadeInDown');
        }
        else{
            $('body.scroll').addClass('no_scroll');
            $('body').removeClass('scroll');

            $('.float_menu .rates_links').removeClass('animated fadeInDown');

        }
    });

    $('.domains_tabs .tab').click(function() {
        $('.domains_tabs .tab').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('id');
        $('.domains_tabs .tab_block').fadeOut( 0, function() { });
        $('.domains_tabs .tab_block.id_' + id).fadeIn( 600, function() { });
    });

    $('.has_children').click(function() {
        $(this).find('.sub_menu').stop().slideToggle('normal');
    });

    $('.rates_links').on("click", "a.inner_anchors", function (event) {
        var id;
        event.preventDefault();
        id = $(this).attr('href').replace(/[^#]*(.*)/, '$1');
        var top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 500);
    });
});

/* Создание калькулятора */

// $(document).ready(function () {
//     qwer();
// });
//
// $("input.dataInput").on('input', function () {
//     di = $(this).val();
//     parseFloat(di);
//     if (document.getElementsByClassName('range01').length > 0) {
//         $(".range01").slider("option", "values", [0, di]);
//     }
//
//     if (di >= +summin && di < sumsecond) {
//         percent = percentfirst;
//     } else if (di >= +sumsecond && di <= sumthird) {
//         percent = percentsecond;
//     } else if (di >= +sumthird && di < summax) {
//         percent = percentthird;
//     } else {
//         percent = percentlast;
//     }
//
//     qwer();
// });
//
// $("input.daysInput").on('input', function () {
//     days = $(this).val();
//     parseInt(days);
//
//     if (di >= +summin && di < sumsecond) {
//         percent = percentfirst;
//     } else if (di >= +sumsecond && di <= sumthird) {
//         percent = percentsecond;
//     } else if (di >= +sumthird && di < summax) {
//         percent = percentthird;
//     } else {
//         percent = percentlast;
//     }
//
//     qwer();
// });

// function qwer() {
//     di = $('input.dataInput').val();
//     days = $('input.daysInput').val();
//
//     var per        = percent * days;
//     profit     = di * per / 100;
//     hashpower  = profit * rate;
//
//     $('.percent').html(per.toFixed(2) + '% of investment');
//     $('.profit').html(+profit.toFixed(2) + ' USD');
//     $('.hashpower').html(+hashpower.toFixed(2) + ' GH/s');
//     $('.days').html(+days);
// };
//
// if (document.getElementsByClassName('range01').length > 0) { // те такие блоки есть
//     function range(min, max) {
//         $(".range01").slider({
//             range: true,
//             min: min,
//             max: max,
//             disabled: true
//         });
//     }
//     range(summin, summax);
// }